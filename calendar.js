
function initCalendar() {

    let xhrEvents = new XMLHttpRequest();
    xhrEvents.open('GET', 'requests/getAllEvents.php', true);

    xhrEvents.onreadystatechange = function () {
        if (xhrEvents.readyState === 4) {
            const res = JSON.parse(xhrEvents.responseText);
            if (xhrEvents.status === 200 && xhrEvents.responseText !== '') {
                fillEventsList(res);

                // GET NEXT AND LAST GAMES
                getNextAndLastMatch();
                generateNextAndLastMatch();

                // CREATE HTML
                generateCalendar();
            }
        }
        else {
            console.log('Erreur serveur');
        }
    }
    xhrEvents.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhrEvents.send('');

}

function fillEventsList(initialEventsList) {

    initialEventsList.forEach(initialEvent => {
        sortedEventList.push({
            id: initialEvent.id,
            name: initialEvent.name,
            date: new Date(initialEvent.date),
            type: initialEvent.type,
            opponent: initialEvent.opponent,
            place: initialEvent.place,
            scoreDom: parseInt(initialEvent.scoreDom, 10),
            scoreExt: parseInt(initialEvent.scoreExt, 10),
            players: JSON.parse(initialEvent.players),
            scorers: JSON.parse(initialEvent.scorers),
            assisters: JSON.parse(initialEvent.assisters)
        })
    });
    sortedEventList.sort(
        (a, b) => {
            return a.date.getTime() - b.date.getTime();
        }
    );
    console.log('Events --> ', sortedEventList);

}


function showCalendarPanel() {
    if (document.querySelector('#calendar').style.display === 'none') {

        // HIDE OTHERS
        document.querySelector('#players').style.display = 'none';
        document.querySelector('#card').style.display = 'none';
        document.querySelector('.selected').classList.remove('selected');

        document.querySelector('#calendar').style.display = 'block';
        document.querySelector('#calendarTitle').classList.add('selected');
    }
}
function generateCalendar() {

    sortedEventList.forEach(event => {

        // CREATE HTML OBJECTS
        let eventEl = document.createElement('div');
        let eventIcon = document.createElement('img');
        let dateSpan = document.createElement('span');
        let deleteIcon = document.createElement('img');
        let editIcon = document.createElement('img');

        // GET VALUES FROM CURSOR AND SET IT
        dateSpan.textContent = getFormatedDate(event.date);
        dateSpan.classList.add('date');
        eventIcon.src = setEventIconSrc(event);
        eventIcon.classList.add('eventIcon');
        deleteIcon.src = 'res/removeIcon.png';
        deleteIcon.onclick = deleteCalendarEvent;
        deleteIcon.classList.add('deleteIcon');
        editIcon.src = 'res/editIcon.png';
        editIcon.classList.add('editIcon');
        editIcon.onclick = () => {
            editCalendarEvent(event);
        }

        if (event.date.getTime() < new Date().getTime()) {
            eventEl.classList.add('pastEvent');
            eventEl.onclick = () => {
                openEvent(event, eventEl);
            }
        }

        eventEl.appendChild(eventIcon);

        if (isAMatch(event.type)) {

            let opponentSpan = document.createElement('span');
            if (event.place === 'Domicile') {
                opponentSpan.textContent = 'Churras ' + event.scoreDom + ' - ' + event.scoreExt + ' ' + event.opponent;
            } else {
                opponentSpan.textContent = event.opponent + ' ' + event.scoreDom + ' - ' + event.scoreExt + ' Churras';
            }
            opponentSpan.classList.add('opponent');
            eventEl.appendChild(opponentSpan);

        } else {

            let nameSpan = document.createElement('span');
            nameSpan.textContent = event.name;
            nameSpan.classList.add('name');
            eventEl.appendChild(nameSpan);
        }

        eventEl.appendChild(deleteIcon);
        eventEl.appendChild(editIcon);
        eventEl.appendChild(dateSpan);
        eventListEl.appendChild(eventEl);

        // SET ATTRIBUTES TO EVENT
        eventEl.classList.add('event');
        eventEl.classList.add(event.type);
        eventEl.setAttribute('calendarEventId', event.id);

        // IF EVENT IS LAST EVENT --> PLACE NOW
        if (event === lastEvent) {
            generateNow();
        }

    });

}

function getFormatedDate(date) {
    let month = date.getMonth();
    let day = date.getDate();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return day + "/" + month + " - " + hours + ":" + minutes;
}

function generateNow() {
    nowEl = document.createElement('div');
    nowEl.id = 'now';
    eventListEl.appendChild(nowEl);
}
function setEventIconSrc(event) {
    switch (event.type.toLowerCase()) {
        case 'amical':
            return 'res/amicalIcon.png';
        case 'championnat':
            return 'res/leagueIcon.png';
        case 'coupe':
            return 'res/trophyIcon.png';
        case 'entrainement':
            return 'res/weightIcon.png';
        case 'autre':
            return 'res/eventIcon.png';
        default:
            console.log('No Icon found - ' + event.type);
    }
}

function deleteCalendarEvent(e) {
    let eventId = Number(e.target.parentNode.getAttribute('calendarEventId'));

    e.target.parentNode.parentNode.removeChild(e.target.parentNode);

    console.log('ID --> ', eventId);

    $.ajax({
        url: 'requests/deleteEvent.php',
        method: 'post',
        data: {
            id: eventId,
        },
        success: function (response) {
            console.log('OK');
        }
    });
}
