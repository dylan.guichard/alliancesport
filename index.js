window.onload = init;

let calendarDb, calendarEl, eventListEl, addMatchFormEl, addOtherEventFormEl;
let playersDb, playersList;
let lastMatchEl, nextMatchEl;
let type;
let sortedEventList = [];
let lastMatch, nextMatch, lastEvent, nextEvent;
let todayDate;
let currentEvent;
let adminClicker = 0;
let adminMode = "false";
let screenSize;

function init() {

    initCalendar();
    initPlayers();

    initVar();

}

function initVar() {

    calendarEl = document.querySelector('div#calendar');
    eventListEl = calendarEl.querySelector('div#eventList');
    addMatchFormEl = calendarEl.querySelector('form#addMatch');
    addOtherEventFormEl = calendarEl.querySelector('form#addOtherEvent');

    lastMatchEl = document.querySelector('div#lastMatch');
    nextMatchEl = document.querySelector('div#nextMatch');

    todayDate = new Date();
	
	screenSize = getScreenSize();
}

function displayAddEventForm(_type) {
    type = _type;

    addMatchFormEl.style.display = 'none';
    addOtherEventFormEl.style.display = 'none';

    if (isAMatch(type)) {
        addMatchFormEl.style.display = 'block';
        addMatchFormEl.setAttribute('class', 'addCalendarEntry');
        addMatchFormEl.classList.add(type);
        addMatchFormEl.querySelector('#eventType').value = type;
    } else {
        addOtherEventFormEl.style.display = 'block';
        addOtherEventFormEl.setAttribute('class', 'addCalendarEntry');
        addOtherEventFormEl.classList.add(type);
        addOtherEventFormEl.querySelector('#eventType').value = type;
    }

    console.log(type);
}

function getNextAndLastMatch() {
    const todayTimestamp = todayDate.getTime();
    for (let i = 0; i < sortedEventList.length; i++) {
        event = sortedEventList[i];
        const eventTimestamp = event.date.getTime();
        if (eventTimestamp >= todayTimestamp) {
            // GET LAST
            if (sortedEventList.indexOf(event) != 0) {
                lastEvent = sortedEventList[sortedEventList.indexOf(event) - 1];
                lastMatch = getLastMatch(lastEvent);
            } else {
                lastEvent = 'No last event';
                lastMatch = 'No last match';
            }

            // GET NEXT
            nextEvent = event;
            nextMatch = getNextMatch(nextEvent);
            break;
        }
    }
    if (!nextEvent) {
        if (sortedEventList.length > 0) {
            lastEvent = sortedEventList[sortedEventList.length - 1];
            lastMatch = getLastMatch(lastEvent);
            nextEvent = 'No next event';
            nextMatch = 'No next match';
        } else {
            // NO EVENT IN THE LIST
            console.error('No event in list');
            lastEvent = 'No last event';
            lastMatch = 'No last match';
            nextEvent = 'No next event';
            nextMatch = 'No next match';
        }
    }
}
function getLastMatch(previousEvent) {

    if (previousEvent) {
        if (previousEvent.type.toLowerCase() === "championnat" || previousEvent.type.toLowerCase() === "coupe" || previousEvent.type.toLowerCase() === "amical") {
            return previousEvent;
        } else {
            if (sortedEventList.indexOf(previousEvent) != 0) {
                previousEvent = sortedEventList[sortedEventList.indexOf(previousEvent) - 1];
                return getLastMatch(previousEvent);
            } else {
                return 'No last match';
            }
        }
    }
}
function getNextMatch(nextEvent) {

    if (nextEvent) {
        if (nextEvent.type.toLowerCase() === "championnat" || nextEvent.type.toLowerCase() === "coupe" || nextEvent.type.toLowerCase() === "amical") {
            return nextEvent;
        } else {
            if (sortedEventList.indexOf(nextEvent) != sortedEventList.length) {
                nextEvent = sortedEventList[sortedEventList.indexOf(nextEvent) + 1];
                return getNextMatch(nextEvent);
            } else {
                return 'No last match';
            }
        }
    }
}
function generateNextAndLastMatch() {

    if (lastMatch && lastMatch.date) {
        generateMatch(lastMatchEl, lastMatch);
    } else if (lastMatch === 'No last match') {
        lastMatchEl.querySelector('.matchName').textContent = 'Pas de match à passé';
    }

    if (nextMatch && nextMatch.date) {
        generateMatch(nextMatchEl, nextMatch);
    } else if (nextMatch === 'No next match') {
        nextMatchEl.querySelector('.matchName').textContent = 'Pas de match à venir';
    }
}
function generateMatch(targetEl, match) {
    let matchName;
    if (match.place === 'Domicile') {
        matchName = 'Churras ' + match.scoreDom + ' - ' + match.scoreExt + ' ' + match.opponent
    } else {
        matchName = match.opponent + ' ' + match.scoreDom + ' - ' + match.scoreExt + ' Churras'
    }

    targetEl.querySelector('.matchName').textContent = matchName;
    targetEl.querySelector('.matchTime').textContent = match.eventTime;
    targetEl.querySelector('.matchDate').textContent = match.date.toLocaleDateString();
}

function isAMatch(type) {
    if (type.toLowerCase() === 'championnat' || type.toLowerCase() === 'coupe' || type.toLowerCase() === 'amical') {
        return true;
    } else {
        return false;
    }
}

function toogleAdminMode() {
    adminClicker++;
    if (adminClicker === 3) {
        adminMode = !adminMode;
        document.querySelector('#main').classList.toggle('admin');
        document.querySelector('#eventPopIn').classList.toggle('admin');
    }

    setTimeout(() => {
        adminClicker = 0;
    }, 3000);
}

function getScreenSize () {
	let screenWidth = window.screen.width;
	if(screenWidth > 1280) {
		return 'large';
	} else if (screenWidth > 700) {
		return 'medium';
	} else {
		return 'small';
	}
}



function ajaxPost(url, data, callback) {
    var req = new XMLHttpRequest();
    req.open("POST", url);
    req.addEventListener("load", function () {
        if (req.status >= 200 && req.status < 400) {
            // Appelle la fonction callback en lui passant la réponse de la requête
            callback(req.responseText);
        } else {
            console.error(req.status + " " + req.statusText + " " + url);
        }
    });
    req.addEventListener("error", function () {
        console.error("Erreur réseau avec l'URL " + url);
    });
    req.send(data);
}