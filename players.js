function initPlayers() {
    
    // PLAYERS
    let xhrPlayers = new XMLHttpRequest();
    xhrPlayers.open('GET', 'requests/getAllPlayers.php', true);

    xhrPlayers.onreadystatechange = function() {
        if ( xhrPlayers.readyState === 4 ) {
            const res = JSON.parse(xhrPlayers.responseText);
            if ( xhrPlayers.status === 200 && xhrPlayers.responseText !== '') {
                fillPlayersList(res);
                generatePlayersListEl('match');
            }
        }
        else {
            console.log('Erreur serveur');
        }
    }
    xhrPlayers.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 

    xhrPlayers.send('');
}


function showPlayersPanel() {
    
    if(document.querySelector('#players').style.display !== 'block') {

        // HIDE OTHERS
        document.querySelector('#calendar').style.display = 'none';

        document.querySelector('.selected').classList.remove('selected');

        document.querySelector('#players').style.display = 'block';
        document.querySelector('#playersTitle').classList.add('selected');
    }

}
function fillPlayersList(initialPlayersList) {
    playersList = [];
    initialPlayersList.forEach(initialPlayer => {

        // FILL EVENT LIST NOT SORTED YET
        playersList.push({
            playerId: parseInt(initialPlayer.id, 10),
            firstName: initialPlayer.firstName,
            lastName: initialPlayer.lastName,
            email: initialPlayer.email,
            poste: initialPlayer.poste,
            age: initialPlayer.age,
            nbMatchTotal: parseInt(initialPlayer.nbMatchTotal, 10),
            nbMatchCup: parseInt(initialPlayer.nbMatchCup, 10),
            nbMatchLeague: parseInt(initialPlayer.nbMatchLeague, 10),
            nbMatchExibition: parseInt(initialPlayer.nbMatchExibition, 10),
            nbGoalTotal: parseInt(initialPlayer.nbGoalTotal, 10),
            nbGoalCup: parseInt(initialPlayer.nbGoalCup, 10),
            nbGoalLeague: parseInt(initialPlayer.nbGoalLeague, 10),
            nbGoalExibition: parseInt(initialPlayer.nbGoalExibition, 10),
            nbAssistTotal: parseInt(initialPlayer.nbAssistTotal, 10),
            nbAssistCup: parseInt(initialPlayer.nbAssistCup, 10),
            nbAssistLeague: parseInt(initialPlayer.nbAssistLeague, 10),
            nbAssistExibition: parseInt(initialPlayer.nbAssistExibition, 10),
            stats: {
                vit: parseInt(initialPlayer.vit, 10),
                dri: parseInt(initialPlayer.dri, 10),
                pas: parseInt(initialPlayer.pas, 10),
                def: parseInt(initialPlayer.def, 10),
                phy: parseInt(initialPlayer.phy, 10),
                tir: parseInt(initialPlayer.tir, 10)
            },
            imgPath: initialPlayer.imgPath,
            nbRedCardCup: parseInt(initialPlayer.nbRedCardCup, 10),
            nbRedCardExibition: parseInt(initialPlayer.nbRedCardExibition, 10),
            nbRedCardLeague: parseInt(initialPlayer.nbRedCardLeague, 10),
            nbRedCardTotal: parseInt(initialPlayer.nbRedCardTotal, 10),
            nbYellowCardCup: parseInt(initialPlayer.nbYellowCardCup, 10),
            nbYellowCardExibition: parseInt(initialPlayer.nbYellowCardExibition, 10),
            nbYellowCardLeague: parseInt(initialPlayer.nbYellowCardLeague, 10),
            nbYellowCardTotal: parseInt(initialPlayer.nbYellowCardTotal, 10)
        });
    });
    console.log('Players --> ', playersList);
}
function generatePlayersListEl(sortingType) {
    
    const playersListEl = document.querySelector('#sortedPlayersList');
    playersListEl.innerHTML = '';

    sortPlayerListBy(sortingType);

    playersList.forEach(player => {
        
        const playerEl = document.createElement('div');
        playerEl.classList.add('player');

        playerNameEl = document.createElement('span');
        playerNameEl.classList.add('playerNameInList');
        playerNameEl.textContent = player.firstName + ' ' + player.lastName;
        playernbGoalEl = document.createElement('span');
        playernbGoalEl.classList.add('playerStatInList');
        playernbGoalEl.textContent = player.nbGoalTotal;
        playernbAssistEl = document.createElement('span');
        playernbAssistEl.classList.add('playerStatInList');
        playernbAssistEl.textContent = player.nbAssistTotal;
        playerNbMatchEl = document.createElement('span');
        playerNbMatchEl.classList.add('playerStatInList');
        playerNbMatchEl.textContent = player.nbMatchTotal;

        playerEl.appendChild(playerNameEl);
        playerEl.appendChild(playerNbMatchEl);
        playerEl.appendChild(playernbGoalEl);
        playerEl.appendChild(playernbAssistEl);
        playerEl.addEventListener('click', function(e) {
            initPlayerCard(player, e);
        }, false);
        playersListEl.appendChild(playerEl);
    });

}

function showAddPlayerForm() {
    document.querySelector('#addPlayerForm').style.display = 'block';
}

function addPlayerSuccess(reponse) {
    console.log('Add player success -> ', reponse);
}

function savePlayersActions() {
    
    
    currentEvent.scoreDom = Number(document.querySelector('input.scoreDom').value);
    currentEvent.scoreExt = Number(document.querySelector('input.scoreExt').value);

    $.ajax({
        url      : 'requests/updatePlayersActions.php',
        method   : 'post', 
        data     : {
            eventId: currentEvent.id,
            players: JSON.stringify(currentEvent.players),
            scorers: JSON.stringify(currentEvent.scorers),
            assisters: JSON.stringify(currentEvent.assisters)
        },
        success  : function(response){
          console.log('players saved');
          saveEvent()
        }
    });

}

function sortPlayerListBy(sortingType) {
    
    playersList.sort(
        (a, b) => {
            if(sortingType === 'match') {
                return b.nbMatchTotal - a.nbMatchTotal;
            } else if (sortingType === 'goal') {
                return b.nbGoalTotal - a.nbGoalTotal;
            } else if (sortingType === 'assist') {
                return b.nbAssistTotal - a.nbAssistTotal;
            }
        }
    );
}