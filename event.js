// --- EVENT --- //
function openEvent(event, eventEl) {
    generateEventDetails(event, eventEl);
}



// --- EVENT DETAILS ---//
function generateEventDetails(event, eventEl) {
    currentEvent = event;

    if (document.querySelector('.selectedEvent')) {
        removeOldEventDetails();
    }
    eventEl.classList.add('selectedEvent');


    generateEventDetailsHeader();
    generateEventDetailsPlayers(event);
    generateEventDetailsStyle()

}
function generateEventDetailsHeader() {
    const eventHeaderEl = document.querySelector('#eventHeader');

    // ADD HEADER
    let eventTitleEl = document.createElement('div');
    eventTitleEl.classList.add('eventTitle');
    if (currentEvent.place === 'Domicile') {
        eventTitleEl.innerText = "Churras " + currentEvent.scoreDom + " - " + currentEvent.scoreExt + " " + currentEvent.opponent;
    } else {
        eventTitleEl.innerText = currentEvent.opponent + " " + currentEvent.scoreDom + " - " + currentEvent.scoreExt + " Churras";
    }

    let eventDateEl = document.createElement('div');
    eventDateEl.classList.add('eventDate');
    eventDateEl.innerText = getFormatedDate(currentEvent.date);

    eventHeaderEl.appendChild(eventTitleEl);
    eventHeaderEl.appendChild(eventDateEl);
}
function generateEventDetailsPlayers(event) {
    const eventPlayersEl = document.querySelector('#eventPlayers');
    const eventScorersAssistersEl = document.querySelector('#eventScorersAssisters');
    const goalEl = document.createElement('img');
    goalEl.src = 'res/leagueIcon.png'
    goalEl.classList.add('icon');
    goalEl.classList.add('goalIcon');
    const assistEl = document.createElement('img');
    assistEl.src = 'res/assistIcon.png'
    assistEl.classList.add('icon');

    // ADD PLAYERS
    event.players.forEach(playerId => {

        const player = playersList.find(player => player.playerId === playerId);

        const playerEl = document.createElement('div');
        playerEl.classList.add('player');

        const playerNameEl = document.createElement('span');
        playerNameEl.textContent = player.firstName + ' ' + player.lastName;
        playerNameEl.classList.add('playerName');

        const scorerAssisterEl = playerEl.cloneNode(true);

        // IF THE PLAYER IS A SCORER OR AN ASSISTER
        if (event.scorers.find(eventPlayerId => eventPlayerId === player.playerId) || event.assisters.find(eventPlayerId => eventPlayerId === player.playerId)) {

            scorerAssisterEl.appendChild(playerNameEl.cloneNode(true));

            const nbOfGoals = getNbOfGoalsByPlayer(player);
            const nbOfAssists = getNbOfAssistsByPlayer(player);

			let goalsAndAssistsContainerEl = document.createElement('span');
			goalsAndAssistsContainerEl.classList.add('goalsAndAssists');

			if(nbOfGoals > 0) {
				let goalsContainerEl = document.createElement('span');
				goalsContainerEl.classList.add('goals');
				for (let i = 0; i < nbOfGoals; i++) {
					goalsContainerEl.appendChild(goalEl.cloneNode());
				}
				goalsAndAssistsContainerEl.appendChild(goalsContainerEl);
			}
			if(nbOfAssists > 0) {
				let assistsContainerEl = document.createElement('span');
				assistsContainerEl.classList.add('assists');
				for (let i = 0; i < nbOfAssists; i++) {
					assistsContainerEl.appendChild(assistEl.cloneNode());
				}
				goalsAndAssistsContainerEl.appendChild(assistsContainerEl);
			}

			scorerAssisterEl.appendChild(goalsAndAssistsContainerEl);
            eventScorersAssistersEl.appendChild(scorerAssisterEl);
        }

        playerEl.appendChild(playerNameEl);
        eventPlayersEl.appendChild(playerEl);
    });
}
function generateEventDetailsStyle() {
    const eventDetailsEl = document.querySelector('#eventDetails');

    eventDetailsEl.style.display = "block";

    // SET TOP
    if (screenSize === 'large') {
        eventDetailsEl.style.top = document.querySelector('#calendar').offsetTop + 'px';
    } else {
        selectedEvent = document.querySelector('.selectedEvent');
        eventDetailsEl.style.top = (selectedEvent.offsetTop + selectedEvent.offsetHeight) + 'px';
        selectedEvent.style.marginBottom = (eventDetailsEl.offsetHeight + 20) + 'px';
    }
}
function removeOldEventDetails() {

    // REMOVE CONTENT
    removeOldPlayersInEventDetails();

    // REMOVE STYLE
    document.querySelector('.selectedEvent').style.marginBottom = '0px';
    document.querySelector('.selectedEvent').classList.remove('selectedEvent');
}
function removeOldPlayersInEventDetails() {
    // REMOVE HEADER
    while (document.querySelector('#eventHeader').firstChild) {
        document.querySelector('#eventHeader').removeChild(document.querySelector('#eventHeader').firstChild);
    }

    // REMOVE PLAYERS
    let oldPlayersEl = document.querySelector('#eventPlayers').querySelectorAll('.player');
    oldPlayersEl.forEach(oldPlayerEl => {
        document.querySelector('#eventPlayers').removeChild(oldPlayerEl);
    });

    // REMOVE SCORERS & ASSISTERS
    let oldScorersAssistersEl = document.querySelector('#eventScorersAssisters').querySelectorAll('.player');
    oldScorersAssistersEl.forEach(oldScorerAssisterEl => {
        document.querySelector('#eventScorersAssisters').removeChild(oldScorerAssisterEl);
    });
}



// --- EDIT EVENT --- //
function editCalendarEvent(event) {
    currentEvent = event;
    console.log(event);
    document.querySelector('#eventPopIn').style.display = 'block';
    document.querySelector('#filter').style.display = 'block';

    generateWatchMode();
    generateEditMode();
}
function generateWatchMode() {
    document.querySelector('#watchMode .eventPlayersList').innerHTML = '';
    if (currentEvent.place === 'Domicile') {
        document.querySelector('#watchMode .teamDom').textContent = 'Churras';
        document.querySelector('#watchMode .teamExt').textContent = currentEvent.opponent;
    } else {
        document.querySelector('#watchMode .teamDom').textContent = currentEvent.opponent;
        document.querySelector('#watchMode .teamExt').textContent = 'Churras';
    }
    document.querySelector('#watchMode .scoreDom').textContent = currentEvent.scoreDom;
    document.querySelector('#watchMode .scoreExt').textContent = currentEvent.scoreExt;

    // ADD PLAYERS
    const eventPlayersListEl = document.querySelector('#eventPopIn #watchMode .eventPlayersList');
    currentEvent.players.forEach(playerId => {

        const player = playersList.find(player => player.playerId === playerId);

        const playerEl = document.createElement('div');
        playerEl.classList.add('player');

        const playerNameEl = document.createElement('span');
        playerNameEl.textContent = player.firstName + ' ' + player.lastName;
        playerNameEl.classList.add('playerName');
        playerEl.appendChild(playerNameEl);

        // IF THE PLAYER IS A SCORER
        if (currentEvent.scorers.find(eventPlayerId => eventPlayerId === player.playerId)) {
            const nbOfGoal = getNbOfGoalsByPlayer(player);
            for (let i = 0; i < nbOfGoal; i++) {
                const goalIcon = document.createElement('img');
                goalIcon.src = 'res/leagueIcon.png'
                goalIcon.classList.add('goalIcon');
                playerEl.appendChild(goalIcon);
            }
        }
        // IF THE PLAYER IS AN ASSISTER
        if (currentEvent.assisters.find(eventPlayerId => eventPlayerId === player.playerId)) {
            const nbOfAssists = getNbOfAssistsByPlayer(player);
            for (let i = 0; i < nbOfAssists; i++) {
                const assistIcon = document.createElement('img');
                assistIcon.src = 'res/assistIcon.png'
                assistIcon.classList.add('assistIcon');
                playerEl.appendChild(assistIcon);
            }
        }

        eventPlayersListEl.appendChild(playerEl);
    });
}
function generateEditMode() {

    document.querySelector('#editMode .eventPlayersList').innerHTML = '';
    document.querySelector('#editMode .otherPlayersList').innerHTML = '';
    if (currentEvent.place === 'Domicile') {
        document.querySelector('#editMode .teamDom').textContent = 'Churras';
        document.querySelector('#editMode .teamExt').textContent = currentEvent.opponent;
    } else {
        document.querySelector('#editMode .teamDom').textContent = currentEvent.opponent;
        document.querySelector('#editMode .teamExt').textContent = 'Churras';
    }
    document.querySelector('#editMode .scoreDom').value = currentEvent.scoreDom;
    document.querySelector('#editMode .scoreExt').value = currentEvent.scoreExt;

    // GENERATE THE BOTH LISTS OF PLAYERS

    const eventPlayersListEl = document.querySelector('#eventPopIn #editMode .eventPlayersList');
    const otherPlayersListEl = document.querySelector('#eventPopIn #editMode .otherPlayersList');

    playersList.forEach(player => {

        const playerEl = document.querySelector('#eventPopIn #editMode div.player.hidden').cloneNode(true);
        playerEl.classList.remove('hidden');

        const playerNameEl = playerEl.querySelector('span.playerName');
        playerNameEl.textContent = player.firstName + ' ' + player.lastName;
        playerNameEl.onclick = () => switchEventPlayerList(player, playerEl);


        // IF THE PLAYER IS A SCORER
        if (currentEvent.scorers.find(eventPlayerId => eventPlayerId === player.playerId)) {
            const nbOfGoal = getNbOfGoalsByPlayer(player);
            playerEl.querySelector('.nbGoal').textContent = nbOfGoal;
        }
        // IF THE PLAYER IS AN ASSISTER
        if (currentEvent.assisters.find(eventPlayerId => eventPlayerId === player.playerId)) {
            const nbOfAssists = getNbOfAssistsByPlayer(player);
            playerEl.querySelector('.nbAssist').textContent = nbOfAssists;
        }

        // IF THE PLAYER IS IN THE MATCH
        if (currentEvent.players.find(eventPlayerId => eventPlayerId === player.playerId)) {
            eventPlayersListEl.appendChild(playerEl);
        } else {
            otherPlayersListEl.appendChild(playerEl);
        }

    });
}
function switchEventPlayerList(player, playerEl) {

    playerEl.parentElement.removeChild(playerEl);
    if (currentEvent.players.find(eventPlayerId => eventPlayerId === player.playerId)) {
        const playerIndex = currentEvent.players.findIndex(eventPlayerId => eventPlayerId === player.playerId);
        currentEvent.players.splice(playerIndex, 1);
        document.querySelector('#eventPopIn #editMode .otherPlayersList').appendChild(playerEl);
    } else {
        currentEvent.players.push(player.playerId);
        document.querySelector('#eventPopIn #editMode .eventPlayersList').appendChild(playerEl);

    }
    console.log('SWITCh --> ', player);
}
function switchEventMode() {
    document.querySelector('#watchMode').classList.toggle('hidden');
    document.querySelector('#watchMode').classList.toggle('show');
    document.querySelector('#editMode').classList.toggle('hidden');
    document.querySelector('#editMode').classList.toggle('show');
}
function saveEvent() {

    currentEvent.scoreDom = Number(document.querySelector('input.scoreDom').value);
    currentEvent.scoreExt = Number(document.querySelector('input.scoreExt').value);

    $.ajax({
        url: 'requests/updateMatch.php',
        method: 'post',
        data: {
            id: currentEvent.id,
            scoreDom: currentEvent.scoreDom,
            scoreExt: currentEvent.scoreExt,
            players: JSON.stringify(currentEvent.players),
            scorers: JSON.stringify(currentEvent.scorers),
            assisters: JSON.stringify(currentEvent.assisters)
        },
        success: function (response) {
            console.log('launch player save');
        }
    });

}
function closeEvent() {
    document.querySelector('#eventPopIn').style.display = 'none';
    document.querySelector('#filter').style.display = 'none';
}


// GETTER
function getNbOfGoalsByPlayer(player) {
    nbOfGoal = 0;
    currentEvent.scorers.forEach(scorerId => {
        if (scorerId === player.playerId) {
            nbOfGoal++;
        }
    });
    return nbOfGoal;
}
function getNbOfAssistsByPlayer(player) {
    nbOfAssists = 0;
    currentEvent.assisters.forEach(assisterId => {
        if (assisterId === player.playerId) {
            nbOfAssists++;
        }
    });
    return nbOfAssists;
}


// GOALS
function addGoals(e) {

    const playerEl = e.target.parentElement.parentElement;
    const playerFullName = playerEl.querySelector('.playerName').textContent;
    const playerFirstName = playerFullName.replace(/ .*/, '');
    const playerLastName = playerFullName.replace(/.* /, '');
    const player = playersList.find(player => player.firstName === playerFirstName && player.lastName === playerLastName);
    currentEvent.scorers.push(player.playerId);

    playerEl.querySelector('.nbGoal').textContent = Number(playerEl.querySelector('.nbGoal').textContent) + 1;
}
function removeGoals(e) {

    const playerEl = e.target.parentElement.parentElement;
    const playerFullName = playerEl.querySelector('.playerName').textContent;
    const playerFirstName = playerFullName.replace(/ .*/, '');
    const playerLastName = playerFullName.replace(/.* /, '');
    const player = playersList.find(player => player.firstName === playerFirstName && player.lastName === playerLastName);
    currentEvent.scorers.splice(currentEvent.scorers.indexOf(player.playerId), 1);

    playerEl.querySelector('.nbGoal').textContent = Number(playerEl.querySelector('.nbGoal').textContent) - 1;
}


// ASSISTS
function addAssists(e) {

    const playerEl = e.target.parentElement.parentElement;
    const playerFullName = playerEl.querySelector('.playerName').textContent;
    const playerFirstName = playerFullName.replace(/ .*/, '');
    const playerLastName = playerFullName.replace(/.* /, '');
    const player = playersList.find(player => player.firstName === playerFirstName && player.lastName === playerLastName);
    currentEvent.assisters.push(player.playerId);

    playerEl.querySelector('.nbAssist').textContent = Number(playerEl.querySelector('.nbAssist').textContent) + 1;
}
function removeAssists(e) {

    const playerEl = e.target.parentElement.parentElement;
    const playerFullName = playerEl.querySelector('.playerName').textContent;
    const playerFirstName = playerFullName.replace(/ .*/, '');
    const playerLastName = playerFullName.replace(/.* /, '');
    const player = playersList.find(player => player.firstName === playerFirstName && player.lastName === playerLastName);
    currentEvent.assisters.splice(currentEvent.assisters.indexOf(player.playerId), 1);

    playerEl.querySelector('.nbAssist').textContent = Number(playerEl.querySelector('.nbAssist').textContent) - 1;
}
