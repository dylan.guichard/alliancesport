let oldSelectedPlayer;

let cardEl, cardInfosEl;

function initPlayerCard(player, e) {
	
    cardEl = document.querySelector('#card').cloneNode(true);
    cardInfosEl = cardEl.querySelector('#cardInfos');
	
    var ctx = cardEl.querySelector('#myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: ['VIT', 'DRI', 'PAS', 'DEF', 'PHY', 'TIR'],
            datasets: [{
                label: '',
                order: 1,
                data: Object.values(player.stats),
                backgroundColor: 'rgba(0, 119, 61, 0.4)',
                borderColor: '#00773d',
                borderWidth: 2,
                pointBorderWidth: 0
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scale: {
                angleLines: {
                    display: false
                },
                gridLines: {
                    display: true
                },
                pointLabels: {
                    fontSize: 15,
                    fontStyle: 'bold'
                },
                ticks: {
                    min: 0,
                    max: 100,
                    stepSize: 100,
                    display: false
                }
            }
        }
    });

	removeOldCard();
    generatePlayerCard(player);
    displayPlayerCard(e);
    setStyle();
}

function removeOldCard() {
	const playerCardEl = document.querySelector('#sortedPlayersList');
	if(playerCardEl.querySelector('#card')) {
		playerCardEl.removeChild(playerCardEl.querySelector('#card'));
	}
}

function setStyle() {

    let selectedPlayerEl = document.querySelector('.selectedPlayer');

	// AFFICHAGE MOBILE
	if (screenSize === 'large') {
		// SET TOP
		const topValue = document.querySelector('#players').offsetTop;
		cardEl.style.top = topValue + 'px';

		// SET BORDER RADIUS
		const borderRadiusValue = cardEl.querySelector("#cardHeader").offsetHeight / 2;
		cardEl.style.borderRadius = borderRadiusValue + "px";
		cardEl.querySelector('#cardHeader').style.borderRadius = borderRadiusValue + "px 0 0 0";
    } else {
		
	}
	

}

function generatePlayerCard(player) {

    console.log('Player -> ', player);

    // NOTES
    cardEl.querySelectorAll('.cardStat').forEach((statBloc, index) => {
        let statType = statBloc.innerText.toLowerCase().replace(/:.*/s, '');
        statBloc.querySelector('.note').innerText = player.stats[statType];
    });

    // NOTE GENERALE
    let noteGen = getGeneralNote(Object.values(player.stats));
    cardEl.querySelector('#cardHeaderInfos .cardNote').innerText = noteGen;

    // POSTE
    cardEl.querySelector('#cardHeaderInfos .cardInfoPoste').innerText = player.poste;
    addPosteClass(cardEl.querySelector('#cardHeaderInfos .cardInfoPoste'), player.poste);

    // AGE
    cardInfosEl.querySelectorAll('span')[0].innerText = player.age + ' ans';

    // STATS
    cardInfosEl.querySelectorAll('span')[1].innerText = player.nbMatchTotal;
    cardInfosEl.querySelectorAll('span')[2].innerText = player.nbGoalTotal;
    cardInfosEl.querySelectorAll('span')[3].innerText = player.nbAssistTotal;

    // PHOTO
    cardEl.querySelector('#cardPhoto').src = player.imgPath;
}

function addPosteClass(el, poste) {
    // REMOVE OLD CLASS
    el.classList.remove('a');
    el.classList.remove('m');
    el.classList.remove('g');
    el.classList.remove('d');

    el.classList.add(poste[0].toLowerCase());

}

function displayPlayerCard(event) {
    let selectedPlayerEl = event.target;
    if (!selectedPlayerEl.classList.contains('player')) {
        selectedPlayerEl = selectedPlayerEl.parentNode;
    }

    if (oldSelectedPlayer) {
        oldSelectedPlayer.classList.remove('selectedPlayer');
    }

    cardEl.style.display = 'flex';
    selectedPlayerEl.classList.add('selectedPlayer');
    oldSelectedPlayer = selectedPlayerEl;
	
	const playersListEl = document.querySelector('#sortedPlayersList');
	const playerIndex = Array.from(selectedPlayerEl.parentNode.children).indexOf(selectedPlayerEl);
	playersListEl.insertBefore(cardEl, playersListEl.childNodes[playerIndex + 1]);
}

function getGeneralNote(notesList) {
    let total = 0;
    notesList.forEach(note => {
        total += parseInt(note);
    });
    return parseInt((total / notesList.length) + 5);
}

let fakeData = {
    "id": 1,
    "age": 25,
    "nbMatchs": 10,
    "nbButs": 5,
    "nbPasses": 2,
    "photo": "res/photo/photo1",
    'stats': {
        "vit": 65,
        "dri": 80,
        "pas": 85,
        "def": 40,
        "phy": 70,
        "tir": 75
    },
    "poste": "AC",
    "age": 25
}