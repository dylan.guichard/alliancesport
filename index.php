<?php

    include("requests/updateMatch.php");
    include("requests/deleteEvent.php");

?>

<html>
    <head>
        <meta charset="utf-8" />
        <title>Churras</title>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="playerCard.css">
        <link media="screen and (max-width: 1280px)" rel="stylesheet" href="responsive.css">
        <script src="index.js"></script>
        <script src="calendar.js"></script>
        <script src="players.js"></script>
        <script src="event.js"></script>
        <script src="playerCard.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    </head>

    <body>
        <?php 
            include("requests/connectionInit.php");
            
            // FORM ADD PLAYER
            if (isset($_POST['playerFirstName'])) {
                include("requests/addNewPlayer.php");
            }
            // FORM ADD MATCH
            if (isset($_POST['opponent'])) {
                include("requests/addNewMatch.php");
            }
            // FORM ADD OTHER EVENT
            if (isset($_POST['eventName'])) {
                include("requests/addOtherEvent.php");
            }
        ?>

        <div id="main">
            <h1 onclick="toogleAdminMode()">Churras</h1>

            <div id="lastNextMatchBloc">
                <div id="lastMatch" class="lastNextMatch">
                    <h2>Dernier Match</h2>

                    <div class="matchBloc">
                        <span class="matchName"></span><!--
                        --><span class="matchMoment">
                            <div class="matchDate"></div>
                            <div class="matchTime"></div>
                        </span>
                    </div>

                </div>

                <div id="nextMatch" class="lastNextMatch">
                    <h2>Prochain Match</h2>            

                    <div class="matchBloc">
                        <span class="matchName"></span><!--
                        --><span class="matchMoment">
                            <div class="matchDate"></div>
                            <div class="matchTime"></div>
                        </span>
                    </div>

                </div>
            </div><!--

            --><div id="mainPanel">
                
                <h2 onclick="showCalendarPanel()" class="selected" id="calendarTitle">Calendrier & Resultats</h2>
                <h2 onclick="showPlayersPanel()" id="playersTitle">Joueurs</h2>
                
                
                <div id="calendar">

                    <div id="addEvent">
                        <button class="championnat" onclick="displayAddEventForm('championnat')">Championnat</button><!--
                        --><button class="coupe" onclick="displayAddEventForm('coupe')">Coupe</button><!--
                        --><button class="amical" onclick="displayAddEventForm('amical')">Amical</button><!--
                        --><button class="entrainement" onclick="displayAddEventForm('entrainement')">Entrainement</button><!--
                        --><button class="autre" onclick="displayAddEventForm('autre')">Evenement</button>
                    </div>
                    <form id="addMatch" class="addCalendarEntry" method="post" action="">
                        <input id="opponent" name="opponent" type="text" placeholder="Adversaire"><!--
                        --><span class="domExt">DOM</span><!--
                        --><label class="switch">
                            <input id="eventPlace" name="place" type="checkbox">
                            <span class="slider round"></span>
                        </label><!--
                        --><span class="domExt">EXT</span><!--
                        --><input id="eventDate" name="date" type="datetime-local"><!--
                        --><input type="submit" value="OK">
                        <input type="hidden" name="type" value="" id="eventType">
                    </form>
                    <form id="addOtherEvent" class="addCalendarEntry" method="post" action="">
                        <input id="eventName" name="eventName" type="text" placeholder="Titre"><!--
                        --><input id="eventDate" name="date" type="datetime-local"><!--
                        --><input type="submit" value="OK">
                        <input type="hidden" name="type" value="" id="eventType">
                    </form>
                    <div id="eventList"></div>
                    <div id="eventDetails">
                        <div id="eventHeader"></div>
                        <div id="eventScorersAssisters"></div>
                        <div id="eventPlayers">
                            <div style="text-align: center; margin-bottom: 10px">Joueurs présents</div>
                        </div>
                    </div>
                </div>
                
                
                <div id="players">

                    <div id="playersList">
                        <div id="playersListHeader">
                            <span class="playerNameInList">Joueur</span><!--
                            --><span class="playerStatInList" onclick="generatePlayersListEl('match')">M</span><!--
                            --><span class="playerStatInList" onclick="generatePlayersListEl('goal')">B</span><!--
                            --><span class="playerStatInList" onclick="generatePlayersListEl('assist')">P</span>
                        </div>
                        <div id="sortedPlayersList"></div>
                    </div>

                    <form id="addPlayerForm" method="post" action="">
                        <input type="text" name="playerFirstName" id="playerFirstNameInput" placeholder="Prénom"><!--
                        --><input type="text" name="playerLastName" id="playerLastNameInput" placeholder="Nom"><!--
                        --><input type="mail" name="playerEmail" id="playerEmailInput" placeholder="Email"><!--
                        --><input type="number" name="playerAge" id="playerAgeInput" placeholder="Age"><!--
                        --><input type="text" name="playerPosition" id="playerPositionInput" placeholder="Pos"><!--
                        --><input type="submit">
                    </form>

                    <button id="addNewPlayer" onclick="showAddPlayerForm()">Ajouter un joueur</button>
                </div>
            </div>

                    
            <?php include("playerCard.html") ?>

            <div id="stats">
            </div>

        </div>


        <div id="eventPopIn">
            <div id="watchMode" class="show">
                <div class="matchTitle">
                    <span class="teamDom team"></span><!--
                    --><span class="scoreDom"></span><!--
                    --> - <!--
                    --><span class="scoreExt"></span><!--
                    --><span class="teamExt team"></span>
                </div>
                <div class="eventPlayersListHeader">
                    <h3>Joueurs présents</h3><!--
                    --><h3>B</h3><!--
                    --><h3>P</h3>
                </div>
                <div class="eventPlayersList">
                    
                </div>
            </div>
            <div id="editMode" class="hidden">
                <div class="matchTitle">
                    <span class="teamDom team"></span>
                    <input class="scoreDom" placeholder="0"></input>
                    - 
                    <input class="scoreExt" placeholder="0"></input>
                    <span class="teamExt team"></span>
                </div>
                <div class="otherPlayersBloc">
                    <div class="eventPlayersListHeader">
                        <h3>Joueurs absents</h3>
                    </div><!--
                    --><div class="otherPlayersList">
                        <h3>Joueurs absents</h3>
                    </div>
                </div><!--
                --><div class="eventPLayersBloc">
                    <div class="eventPlayersListHeader">
                        <h3>Joueurs présents</h3><!--
                        --><h3>B</h3><!--
                        --><h3>P</h3>
                    </div>
                    <div class="eventPlayersList">
                    </div>
                </div>
                <button id="validBtn" onclick="savePlayersActions()">Valider</button>
                
                <div class="player hidden">
                    <span class="playerName"></span><!--
                    --><span class="nbGoalBloc playerStat">
                        <button class="lessGoals" onclick="removeGoals(event)">-</button>
                        <span class="nbGoal">0</span>
                        <button class="plusGoals" onclick="addGoals(event)">+</button>
                    </span><!--
                    --><span class="nbAssistBloc playerStat">
                        <button class="lessAssists" onclick="removeAssists(event)">-</button>
                        <span class="nbAssist">0</span>
                        <button class="plusAssists" onclick="addAssists(event)">+</button>
                    </span>
                    <img src="res/chevron.png" class="chevron" />
                </div>
            </div>

            <img src="res/editIcon.png" class="editIcon" onclick="switchEventMode()">
            <img src="res/removeIcon.png" class="closeIcon" onclick="closeEvent()">
        </div>
        <div id="filter" onclick="closeEvent()"></div>
    </body>

</html>
