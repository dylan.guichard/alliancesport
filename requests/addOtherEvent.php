<?php
	include("connectionInit.php");

    $name = $_POST['eventName'];
    $type = $_POST['type'];

    $date = new DateTime($_POST['date']);

    $req = $bdd->prepare(
        'INSERT INTO events(
            id,
            date,
            name,
            type,
            place,
            opponent,
            scoreDom,
            scoreExt,
            players,
            scorers,
            assisters
        )  VALUES(
            NULL,
            :date,
            :name,
            :type,
            "",
            "",
            0,
            0,
            "[]",
            "[]",
            "[]"
        )'
    );
    $req->execute(array(
        'date' => $date->format('Y-m-d H:i'),
        'type' => $type,
        'name' => $name
        )
    );

    // EMPECHE LE DOUBLE ENVOI AU REFRESH
    unset($_POST);
    header('Location: ./index.php');

?>