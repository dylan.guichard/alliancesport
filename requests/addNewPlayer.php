<?php

    include("connectionInit.php");
    
    $playerFirstName = str_replace(" ", "", $_POST['playerFirstName']);
    $playerLastName = str_replace(" ", "", $_POST['playerLastName']);
    $playerEmail = str_replace(" ", "", $_POST['playerEmail']);
    $playerAge = str_replace(" ", "", $_POST['playerAge']);
    $playerPosition = str_replace(" ", "", $_POST['playerPosition']);

    $req = $bdd->prepare(
        'INSERT INTO players(
            id,
            firstName,
            lastName,
            email,
            poste,
            age,
            nbMatchTotal,
            nbMatchLeague,
            nbMatchCup,
            nbMatchExibition,
            nbGoalTotal,
            nbGoalLeague,
            nbGoalCup,
            nbGoalExibition,
            nbAssistTotal,
            nbAssistLeague,
            nbAssistCup,
            nbAssistExibition,
            nbYellowCardTotal,
            nbYellowCardLeague,
            nbYellowCardCup,
            nbYellowCardExibition,
            nbRedCardTotal,
            nbRedCardLeague,
            nbRedCardCup,
            nbRedCardExibition
        )  VALUES(
            NULL,
            :playerFirstName,
            :playerLastName,
            :playerEmail,
            :playerPosition,
            :playerAge,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        )'
    );
    $req->execute(array(
        'playerFirstName' => $playerFirstName,
        'playerLastName' => $playerLastName,
        'playerEmail' => $playerEmail,
        'playerAge' => $playerAge,
        'playerPosition' => $playerPosition
        )
    );

    // EMPECHE LE DOUBLE ENVOI AU REFRESH
    unset($_POST);
    header('Location: ./index.php');

?>