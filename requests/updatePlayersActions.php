<?php

	include("connectionInit.php");

	if(isset($_POST['players'])){
		
		$eventId = $_POST['eventId'];
		$players = $_POST['players'];
		$scorers = $_POST['scorers'];
		$assisters = $_POST['assisters'];


		// GET OLD EVENT TO COMPARE IT TO SAVED ONE
		$eventReponse = $bdd->query("SELECT * FROM events WHERE id='$eventId'" );
		$oldEvent = $eventReponse->fetch();



		// SPLIT STRING INTO A TABLE
		$oldPlayersStr = str_replace("[", "", $oldEvent['players']);
		$oldPlayersStr = str_replace("]", "", $oldPlayersStr);
		$oldPlayersStr = str_replace(" ", "", $oldPlayersStr);
		$oldPlayersArr = explode(',', $oldPlayersStr);

		$playersStr = str_replace("[", "", $_POST['players']);
		$playersStr = str_replace("]", "", $playersStr);
		$playersStr = str_replace(" ", "", $playersStr);
		$playersArr = explode(',', $playersStr);

		$oldScorersStr = str_replace("[", "", $oldEvent['scorers']);
		$oldScorersStr = str_replace("]", "", $oldScorersStr);
		$oldScorersStr = str_replace(" ", "", $oldScorersStr);
		$oldScorersArr = explode(',', $oldScorersStr);

		$scorersStr = str_replace("[", "", $_POST['scorers']);
		$scorersStr = str_replace("]", "", $scorersStr);
		$scorersStr = str_replace(" ", "", $scorersStr);
		$scorersArr = explode(',', $scorersStr);
		

		$oldAssistersStr = str_replace("[", "", $oldEvent['assisters']);
		$oldAssistersStr = str_replace("]", "", $oldAssistersStr);
		$oldAssistersStr = str_replace(" ", "", $oldAssistersStr);
		$oldAssistersArr = explode(',', $oldAssistersStr);

		$assistersStr = str_replace("[", "", $_POST['assisters']);
		$assistersStr = str_replace("]", "", $assistersStr);
		$assistersStr = str_replace(" ", "", $assistersStr);
		$assistersArr = explode(',', $assistersStr);



		
		// FOREACH OLD PLAYER
		foreach ($oldPlayersArr as $oldPlayerId) {
			
			$nbOfGoals = 0;
			$nbOfAssists = 0;

			// IF PLAYER DOES NOT EXISTS IN NEW EVENT
			if(!in_array($oldPlayerId, $playersArr)) {
				
				// IF WAS A SCORER OR AN ASSISTER
				if(in_array($oldPlayerId, $oldScorersArr)) {
					$nbOfGoals = array_count_values($oldScorersArr)[$oldPlayerId];
				}
				if(in_array($oldPlayerId, $oldAssistersArr)) {
					$nbOfAssists = array_count_values($oldAssistersArr)[$oldPlayerId];
				}
				
				if($oldEvent['type'] == 'Amical') {
					$req = "UPDATE `players` 
						SET `nbMatchTotal` = nbMatchTotal - 1,
						`nbMatchExibition` = nbMatchExibition - 1,
						`nbGoalTotal` = nbGoalTotal - $nbOfGoals,
						`nbGoalExibition` = nbGoalExibition - $nbOfGoals,
						`nbAssistTotal` = nbAssistTotal - $nbOfAssists,
						`nbAssistExibition` = nbAssistExibition - $nbOfAssists
						WHERE `players`.`id` = $oldPlayerId";
				}
				if($oldEvent['type'] == 'championnat') {
					$req = "UPDATE `players` 
						SET `nbMatchTotal` = nbMatchTotal - 1,
						`nbMatchLeague` = nbMatchLeague - 1,
						`nbGoalTotal` = nbGoalTotal - $nbOfGoals,
						`nbGoalLeague` = nbGoalLeague - $nbOfGoals,
						`nbAssistTotal` = nbAssistTotal - $nbOfAssists,
						`nbAssistLeague` = nbAssistLeague - $nbOfAssists
						WHERE `players`.`id` = $oldPlayerId";
				}
				if($oldEvent['type'] == 'coupe') {
					$req = "UPDATE `players` 
						SET `nbMatchTotal` = nbMatchTotal - 1,
						`nbMatchCup` = nbMatchCup - 1,
						`nbGoalTotal` = nbGoalTotal - $nbOfGoals,
						`nbGoalCup` = nbGoalCup - $nbOfGoals,
						`nbAssistTotal` = nbAssistTotal - $nbOfAssists,
						`nbAssistCup` = nbAssistCup - $nbOfAssists
						WHERE `players`.`id` = $oldPlayerId";
				}
				
				$result  = mysqli_query($connection, $req);
				if($result){
					echo 'Player ' . $oldPlayerId . ' removed';
				}
			}
		}



		// FOREACH PLAYER
		foreach ($playersArr as $playerId) {

			$playerChange = false;
			$diffPres = 0;
			$diffGoals = 0;
			$diffAssists = 0;
			$nbOfGoals = 0;
			$oldNbOfGoals = 0;
			$nbOfAssists = 0;
			$oldNbOfAssists = 0;

			// GET NB OF GOALS
			if(in_array($playerId, $scorersArr)) {
				$nbOfGoals = array_count_values($scorersArr)[$playerId];
			}
			if(in_array($playerId, $oldScorersArr)) {
				$oldNbOfGoals = array_count_values($oldScorersArr)[$playerId];
				echo 'array_count_values($oldScorersArr)['.$playerId.'] -> ' . array_count_values($oldScorersArr)[$playerId] . '<br>';
				echo 'OLD SCORER -> ' . $playerId . ' nb de but -> '. $oldNbOfGoals . '<br>';
			}

			// GET NB OF ASSISTS
			if(in_array($playerId, $assistersArr)) {
				$nbOfAssists = array_count_values($assistersArr)[$playerId];
			}
			if(in_array($playerId, $oldAssistersArr)) {
				$oldNbOfAssists = array_count_values($oldAssistersArr)[$playerId];
			}

			// IF PLAYER DOES NOT EXISTS IN OLD EVENT
			if(!in_array($playerId, $oldPlayersArr)) {
				$diffPres = 1;
				$playerChange = true;
			}

			// IF IS A SCORER AND NOT IN OLD SCORER ARR
			if($nbOfGoals != $oldNbOfGoals) {
				$diffGoals = $nbOfGoals - $oldNbOfGoals;
				$playerChange = true;
			}

			// IF IS A ASSISTER AND NOT IN OLD SCORER ARR
			if($nbOfAssists != $oldNbOfAssists) {
				$playerChange = true;
				$diffAssists = $nbOfAssists - $oldNbOfAssists;
			}
			if($oldEvent['type'] == 'amical') {
				$req = "UPDATE `players` 
					SET `nbMatchTotal` = nbMatchTotal + $diffPres,
					`nbMatchExibition` = nbMatchExibition + $diffPres,
					`nbGoalTotal` = nbGoalTotal + $diffGoals,
					`nbGoalExibition` = nbGoalExibition +$diffGoals,
					`nbAssistTotal` = nbAssistTotal + $diffAssists,
					`nbAssistExibition` = nbAssistExibition + $diffAssists
					WHERE `players`.`id` = $playerId";
			}
			if($oldEvent['type'] == 'championnat') {
				$req = "UPDATE `players` 
					SET `nbMatchTotal` = nbMatchTotal + $diffPres,
					`nbMatchLeague` = nbMatchLeague + $diffPres,
					`nbGoalTotal` = nbGoalTotal + $diffGoals,
					`nbGoalLeague` = nbGoalLeague +$diffGoals,
					`nbAssistTotal` = nbAssistTotal + $diffAssists,
					`nbAssistLeague` = nbAssistLeague + $diffAssists
					WHERE `players`.`id` = $playerId";
			}
			if($oldEvent['type'] == 'coupe') {
				$req = "UPDATE `players` 
					SET `nbMatchTotal` = nbMatchTotal + $diffPres,
					`nbMatchCup` = nbMatchCup + $diffPres,
					`nbGoalTotal` = nbGoalTotal + $diffGoals,
					`nbGoalCup` = nbGoalCup +$diffGoals,
					`nbAssistTotal` = nbAssistTotal + $diffAssists,
					`nbAssistCup` = nbAssistCup + $diffAssists
					WHERE `players`.`id` = $playerId";
			}

			// IF PLAYER HAS CHANGE UPDATE IT
			if($playerChange) {
				$result  = mysqli_query($connection, $req);
				if($result){
					echo 'Player ' . $playerId . ' updated';
				}
			}
		}

		$eventReponse->closeCursor();

	}

?>