<?php
    
    include("connectionInit.php");
    
    $opponent = $_POST['opponent'];
    $type = $_POST['type'];

    $date = new DateTime($_POST['date']);

    $place = "";
    if(isset($_POST["place"])) {
        echo 'PLACE -> ' . $_POST["place"];
        $place = "Exterieur";
    } else {
        $place = "Domicile";
    }

    $req = $bdd->prepare(
        'INSERT INTO events(
            id,
            date,
            name,
            type,
            place,
            opponent,
            scoreDom,
            scoreExt,
            players,
            scorers,
            assisters
        )  VALUES(
            NULL,
            :date,
            "",
            :type,
            :place,
            :opponent,
            0,
            0,
            "[]",
            "[]",
            "[]"
        )'
    );
    $req->execute(array(
        'date' => $date->format('Y-m-d H:i'),
        'type' => $type,
        'place' => $place,
        'opponent' => $opponent
        )
    );

    // EMPECHE LE DOUBLE ENVOI AU REFRESH
    unset($_POST);
    header('Location: ./index.php');

?>